-Prototipo de Software: Concessionária de Seminovos Pedro Dionísio Matias

    Você foi contratado pelo Sr. Pedro Dionísio Matias para construir uma aplicação móvel que
ajude os seus funcionários da concessionária de automóveis seminovos a se lembrarem dos
modelos disponíveis para venda. Os funcionários ficarão responsáveis por cadastrar os carros
que chegarem na loja para vender e também por buscar informações destes carros no aplicativo.
A princípio, para adaptação de seus funcionários, o software será simples e composto apenas
de duas funcionalidades, cadastrar e visualizar carros. O aplicativo irá fazer sincronização com
um servidor central, tendo a persistência de dados somente remota.


&Copy; Copyright - Desenvolvido por Lenner Alves 