package com.example.recuperacao_2022_1_lenneralves.model

import java.util.*

data class Cars (
    val marca: String,
    val valor: Int,
    val date: String,
    val descricao: String
        ) {

    companion object{
        fun getCars(): List<Cars>{
            return arrayListOf<Cars>(
                Cars(marca = "SUV", valor = 250000 , date = "11/07/2022", "Carro 4000Km, ano 2022, o modelo certo para quem  gosta de carros altas e espaçosos."),
                Cars(marca = "Sedan", valor = 125000, date = "11/07/2022", "Carro veloz e potente, o modelo para quem gosta de longas viagens em família."),
                Cars(marca = "Hatch", valor = 50000, date = "11/07/2022", "O carro está em ótima condições."),
                Cars(marca = "Camionete", valor = 100000, date = "11/07/2022", "Tração 4x4, forte como um touro e potente como 200 cavalos."),
                Cars(marca = "SUV", valor = 250000, date = "11/07/2022", "Carro 2000Km, ano 2022, o modelo certo para quem  gosta de carros altas e espaçosos."),
                Cars(marca = "Hatch", valor = 50000, date = "11/07/2022", "O carro está em ótima condições."),
                Cars(marca = "Motocicleta", valor = 8500, date = "11/07/2022", "Motocicleta BMW, de 0 a 100 Km em poucos segundos."),
                Cars(marca = "Camionete", valor = 100000, date = "11/07/2022", "Tração 4x4, forte como um touro e potente como 200 cavalos."),
                Cars(marca = "Sedan", valor = 125000, date = "11/07/2022", "Carro veloz e potente, o modelo para quem gosta de longas viagens em família.")
            ).toList()
        }
    }
}