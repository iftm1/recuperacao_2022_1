package com.example.recuperacao_2022_1_lenneralves.ui.components.CarsApp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Arrangement.Top
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.example.recuperacao_2022_1_lenneralves.model.Cars
import com.example.recuperacao_2022_1_lenneralves.R

@Composable
fun CarsItemView(cars: Cars)
{
    ConstraintLayout(
        modifier = Modifier
            .padding(10.dp)
            .fillMaxWidth()
    ){
        val (inicio, marca, valor, date, descricao) = createRefs()
        var isExpanded by remember { mutableStateOf(false) }
        val surfaceColor by animateColorAsState(
            targetValue = if (isExpanded) colorResource(id = R.color.isExpand) else MaterialTheme.colors.surface,
        )

        Box(
            modifier = Modifier
                .padding(0.dp, 0.dp, 7.dp, 0.dp)
                .constrainAs(inicio) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                }
                .wrapContentHeight(
                    align = Alignment.CenterVertically
                )
        ) {
            Image(painter = painterResource(id =
            if(cars.marca == "SUV")
                R.drawable.suv
            else
                if(cars.marca == "Sedan")
                    R.drawable.sedan
            else
                if(cars.marca == "Camionete")
                    R.drawable.camionete
            else
                if(cars.marca == "Hatch")
                    R.drawable.hatch
            else
                R.drawable.moto
            ),
                contentDescription = "Image Of Cars",
                modifier = Modifier
                    .size(125.dp)
                    .clip(CircleShape)
                    .border(3.5.dp, colorResource(id = R.color.red_back_title), CircleShape)
                )
        }

        Text(text = stringResource(id = R.string.Category) + ": " + cars.marca,
            color = MaterialTheme.colors.onSurface,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(top = 16.dp)
                .constrainAs(marca) {
                    top.linkTo(inicio.top)
                    start.linkTo(inicio.end)
                },
            fontSize = 20.sp
        )
        Text(text = stringResource(id = R.string.Value_Car) + ": " + cars.valor.toString(),
            color = MaterialTheme.colors.onSurface,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(top = 16.dp)
                .constrainAs(valor) {
                    top.linkTo(marca.bottom)
                    start.linkTo(marca.start)
                },
            fontSize = 20.sp
        )
        Text(text = stringResource(id = R.string.Date) + ": " + cars.date,
            color = MaterialTheme.colors.onSurface,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(top = 16.dp)
                .constrainAs(date) {
                    top.linkTo(valor.bottom)
                    start.linkTo(valor.start)
                },
            fontSize = 20.sp
        )
        Surface(shape = MaterialTheme.shapes.medium,
            elevation = 1.dp,
            color = surfaceColor,
            modifier = Modifier
                .animateContentSize()
                .padding(10.dp)
                .constrainAs(descricao) {
                    top.linkTo(date.bottom)
                    start.linkTo(inicio.start)
                }
                .clickable { isExpanded = !isExpanded }
        ){
            Text(text = cars.descricao,
                color = MaterialTheme.colors.onSurface,
                textAlign = TextAlign.Justify,
                fontSize = 20.sp,
                modifier = Modifier
                    .padding(16.dp),
                maxLines = if (isExpanded) Int.MAX_VALUE else 1
            )
        }
    }
}


