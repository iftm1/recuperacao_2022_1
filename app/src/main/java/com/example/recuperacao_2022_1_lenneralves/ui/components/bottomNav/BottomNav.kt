package com.example.recuperacao_2022_1_lenneralves.ui.components.bottomNav

import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.example.recuperacao_2022_1_lenneralves.R

@Composable
fun BottomNav(navControl: NavController)
{
    val items = listOf(
        BottomNavItem.Home,
        BottomNavItem.Cadastrar
    )

    BottomNavigation(
        backgroundColor = colorResource(id = R.color.red_back_title)
    ) {
        val navBackStackEntry by navControl.currentBackStackEntryAsState()
        val currentRoute = navBackStackEntry?.destination?.route
        items.forEach{  item ->
            BottomNavigationItem(
                icon = {
                    Icon(painter = painterResource(id = item.IconBottom),
                        contentDescription = stringResource(id = item.TitleBottom)
                    )
                },
                label ={
                    Text(text = stringResource(id = item.TitleBottom),
                        fontSize = 14.sp
                            )
                },
                onClick ={
                    navControl.navigate(item.keyBottom){
                        navControl.graph.startDestinationRoute?.let { route ->
                            popUpTo(route) {
                                saveState = true
                            }
                        }
                        launchSingleTop = true
                        restoreState = true
                    }
                },
                selectedContentColor = MaterialTheme.colors.onPrimary,
                unselectedContentColor = MaterialTheme.colors.onPrimary.copy(0.4f),
                alwaysShowLabel = true,
                selected = currentRoute == item.keyBottom,

            )

        }
    }
}

