package com.example.recuperacao_2022_1_lenneralves.ui.components.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.example.recuperacao_2022_1_lenneralves.R
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Composable
fun cadastrarCarro()
{
    val listMarcas = listOf("Sedan", "SUV", "Camionete", "Hatch", "Motocicleta")
    val states = remember { mutableStateListOf(false, false, false, false, false)}
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.Cinza))
    ) {
        TitleForm()
        Text(text = stringResource(id = R.string.labelCheck),
            modifier = Modifier.padding(5.dp),
            fontSize = 22.sp)
        CreateCheckBox(listMarcas, states)
        TextFieldsForm()
        Button(onClick = { /*TODO*/ },
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth(0.75f)
            .background(colorResource(id = R.color.purple_500))) {
            Text(text = stringResource(id = R.string.addCar),
            color = Color.White,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold)
        }
    }
}

@Composable
private fun TextFieldsForm()
{
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp, 7.dp, 0.dp, 0.dp)
    ) {
        var text by remember { mutableStateOf("") }


        OutlinedTextField(
            value = text,
            onValueChange = {newText ->
                text = newText
            },
            label = {
                Text(text = "Valor:",
                    fontSize = 20.sp)
            },
            leadingIcon = {
                IconButton(onClick = { /*TODO*/ },) {
                    Icon(imageVector = Icons.Filled.ShoppingCart, contentDescription = "Icon Valor")
                }
            },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number,
                imeAction = ImeAction.Go
            ),
            maxLines = 1
        )
        Spacer(modifier = Modifier.height(5.dp))
        OutlinedTextField(
                value = text,
        onValueChange = {newText ->
            text = newText
        },
        label = {
            Text(text = "Data:",
                fontSize = 20.sp)
        },
        leadingIcon = {
            IconButton(onClick = { /* AdicionarHora() */ },) {
                Icon(imageVector = Icons.Filled.DateRange, contentDescription = "Icon date")
            }
        },
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Number,
            imeAction = ImeAction.Go
        ),
        maxLines = 1
        )
        Spacer(modifier = Modifier.height(5.dp))
        OutlinedTextField(
            value = text,
            onValueChange = {newText ->
                text = newText
            },
            label = {
                Text(text = "Descrição:",
                    fontSize = 20.sp)
            },
            leadingIcon = {
                IconButton(onClick = { /*TODO*/ },) {
                    Icon(imageVector = Icons.Filled.Edit, contentDescription = "Icon description")
                }
            },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Go
            ),
            maxLines = 2
        )
    }
}
@Composable
private fun AdicionarHora()
{
   /* val dateTime = LocalDateTime.now()
        .format(DateTimeFormatter.ofPattern("MM dd yyyy"))*/
}
@Composable
private fun TitleForm()
{
    Text(
        text = stringResource(id = R.string.title_form),
        modifier = Modifier
            .padding(10.dp)
            .fillMaxWidth(),
        fontSize = 35.sp,
        textAlign = TextAlign.Center,
        textDecoration = TextDecoration.Underline,
        color = colorResource(id = R.color.red_back_title),
        fontWeight = FontWeight.ExtraBold
    )
}
@Composable
private fun CreateCheckBox(list: List<String> = emptyList(), states: MutableList<Boolean> = mutableListOf())
{
    Column() {
        list.forEachIndexed { index, item ->
            Row(modifier = Modifier.padding(13.dp, 3.dp)){
                Checkbox(checked = states[index], onCheckedChange = { states[index] = it})
                Text(text = item,
                     modifier = Modifier.padding(7.dp, 0.dp),
                     fontSize = 16.sp)
            }
        }
    }
}