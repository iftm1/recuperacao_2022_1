package com.example.recuperacao_2022_1_lenneralves.ui.components.bottomNav

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.recuperacao_2022_1_lenneralves.ui.components.screens.cadastrarCarro
import com.example.recuperacao_2022_1_lenneralves.ui.components.screens.visualizarCarros

@Composable
fun BottomNavGraph(navHostController: NavHostController) {
    NavHost(
        navController = navHostController,
        startDestination = BottomNavItem.Home.keyBottom
    ) {
        composable(BottomNavItem.Home.keyBottom) {
            visualizarCarros()
        }
        composable(BottomNavItem.Cadastrar.keyBottom ) {
            cadastrarCarro()
        }

    }
}

