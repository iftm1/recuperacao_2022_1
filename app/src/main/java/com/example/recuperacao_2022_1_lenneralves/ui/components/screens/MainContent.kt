package com.example.recuperacao_2022_1_lenneralves.ui.components.screens

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.sp
import androidx.navigation.compose.rememberNavController
import com.example.recuperacao_2022_1_lenneralves.R
import com.example.recuperacao_2022_1_lenneralves.ui.components.bottomNav.BottomNav
import com.example.recuperacao_2022_1_lenneralves.ui.components.bottomNav.BottomNavGraph

@Composable
fun MainContent()
{
    val navController = rememberNavController()
    Scaffold(
        topBar = { MainTopBar() },
        bottomBar = {
            BottomNav(navControl = navController)
        }
    ){

            innerPadding ->
        Box(modifier = Modifier.padding(innerPadding)) {
            BottomNavGraph(navHostController = navController)
        }
    }
}

@Composable
private fun MainTopBar()
{
    TopAppBar(
        title = { Text(
                        text = stringResource(id = R.string.title_app),
                        color =
                                if(MaterialTheme.colors.isLight)
                                    Color.Black
                                else
                                    Color.White,
                        fontSize = 20.sp
                )
        },
        backgroundColor = colorResource(id = R.color.red_back_title)
    )
}
