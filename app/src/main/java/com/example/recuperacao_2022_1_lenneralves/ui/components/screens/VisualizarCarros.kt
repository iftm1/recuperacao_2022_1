package com.example.recuperacao_2022_1_lenneralves.ui.components.screens

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.example.recuperacao_2022_1_lenneralves.R
import com.example.recuperacao_2022_1_lenneralves.model.Cars
import com.example.recuperacao_2022_1_lenneralves.ui.components.CarsApp.CarsListView


@Composable
fun visualizarCarros()
{
    val list = Cars.getCars()
    CarsListView(list = list)
}