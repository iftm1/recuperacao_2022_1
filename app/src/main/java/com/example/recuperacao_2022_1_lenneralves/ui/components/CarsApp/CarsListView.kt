package com.example.recuperacao_2022_1_lenneralves.ui.components.CarsApp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.compose.foundation.background
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import com.example.recuperacao_2022_1_lenneralves.R
import com.example.recuperacao_2022_1_lenneralves.model.Cars

@Composable
fun CarsListView(list: List<Cars>)
{
    LazyColumn(
        modifier = Modifier
            .background(colorResource(id = R.color.Cinza))
    ){
        items(list){ cars ->
            CarsItemView(cars = cars)
        }
    }
}

