package com.example.recuperacao_2022_1_lenneralves.ui.components.bottomNav

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.example.recuperacao_2022_1_lenneralves.R

sealed class BottomNavItem(
    val keyBottom: String,
    @DrawableRes val IconBottom: Int,
    @StringRes val TitleBottom: Int
){
    object Home: BottomNavItem("home", R.drawable.ic_home, R.string.visualizar)
    object Cadastrar: BottomNavItem("cadastrar", R.drawable.ic_add, R.string.cadastrar)
}
