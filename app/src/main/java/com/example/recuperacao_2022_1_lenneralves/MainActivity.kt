package com.example.recuperacao_2022_1_lenneralves

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.example.recuperacao_2022_1_lenneralves.ui.components.screens.MainContent
import com.example.recuperacao_2022_1_lenneralves.ui.theme.Recuperacao_2022_1_LennerAlvesTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Recuperacao_2022_1_LennerAlvesTheme {
                MainContent()
            }
        }
    }

}

